#!/usr/bin/env bash
rm -rf public
rm -rf .antora-cache-dir
antora generate site.yml --cache-dir .antora-cache-dir
serve public
