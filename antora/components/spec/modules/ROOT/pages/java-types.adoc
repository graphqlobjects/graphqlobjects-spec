= Petclinic

PetClinic is a well-known example app in the JVM space, originally implemented by Spring.
This specification uses an alternative implementation provided as a link:https://isis.apache.org/tutorials/2.0.0-M6/petclinic/about.html[tutorial] for link:https://isis.apache.org[Apache Isis].

NOTE: this implementation does _not_ (currently) show GraphqlObjects viewer; we provide these instructions just so you can become familiar with the domain.

== Running the Application

* link:https://isis.apache.org/tutorials/2.0.0-M6/petclinic/010-getting-started.html#prereqs[Prereqs]:

** Java 11 (JDK)
** Maven 3.6.x (or later)
** git

* Clone the repo, build and run:
+
[source,bash]
----
git clone https://github.com/apache/isis-app-demo .       # <.>
git checkout tags/08-03-view-model-projecting-an-entity   # <.>
mvn clean install                                         # <.>
mvn -pl webapp spring-boot:run                            # <.>
----
<.> link:https://isis.apache.org/tutorials/2.0.0-M6/petclinic/010-getting-started.html#exercise-1-1-starter-apps-clone-the-repo[Clone] the link:https://github.com/apache/isis-app-demo[repo]
<.> Checkout one of the later link:https://isis.apache.org/tutorials/2.0.0-M6/petclinic/080-view-models.html#solution-3[solution]s
<.> Build
<.> Run

* Navigate to http://localhost:8080.

* Logon using `sven/pass`.

* Once running, use Prototyping > Run Fixture Script to load in some sample data:
+
image::petclinic-home-page.png[]

== UML Class Diagram

The diagram shows the effective domain model implemented by the app:

[plantuml]
----
include::partial$skinparam.adoc[]

package pets {

    enum PetSpecies <<desc>> {
        Dog
        Cat
        Hamster
        Budgerigar
    }

    class Pet <<ppt>> {
        -id
        -version
        ..
        -title()
        ..
        #name
        #notes
        ..
        -petOwner
        -petSpecies
        ..
        #visits: List<Visit>
        #bookVisit(visitAt: LocalDateTime, reason: String): Visit
    }


    class PetOwner <<role>> {
        -id
        -version
        ..
        -title()
        ..
        #lastName
        #firstName
        #phoneNumber
        #emailAddress
        ..
        -pets
        ..
        #updateName(lastName: String, firstName: String)
        ..
        #addPet(name: String, species: PetSpecies)
        #removePet(pet: Pet)
        ..
        #delete()
    }

    class PetOwners {
        #create(lastName: String, firstName: String) : PetOwner
        ..
        #findByNameLike(lastName: String) : List<PetOwner>
        #findByLastName(name: String) : List<PetOwner>
        #litAll(name: String) : List<PetOwner>
    }
}


package visits {

    class Visit <<mi>> {
        -id
        -version
        ..
        -title()
        ..
        -pet
        ..
        #visitAt: LocalDateTime
        #reason: String
    }
}

PetOwners .-> "create" PetOwner
PetOwner *-r--> "0..*" Pet
Visit "   \n*" -r->  Pet
Pet  "*" -u-> PetSpecies
----

== Code Snippets

This section shows the domain classes as implemented in Apache Isis:

[#petowners]
=== PetOwners

The `pets.PetOwners` domain service is defined as:

[source,java]
.pets.PetOwners.java
----
include::{examplesdir}/dom/petowner/PetOwners.java[tags=barebones]
----

[#petowner]
=== PetOwner

The `pets.PetOwner` domain object (an entity) is defined as:

[source,java]
.PetOwner.java
----
include::{examplesdir}/dom/petowner/PetOwner.java[tags=barebones]
----

It also has these contributed members:

* `pets` collection
+
[source,java]
.PetOwner_pets.java
----
include::{examplesdir}/dom/pet/PetOwner_pets.java[tags=barebones]
----

* `addPet` action
+
[source,java]
.PetOwner_addPet.java
----
include::{examplesdir}/dom/pet/PetOwner_addPet.java[tags=barebones]
----

* `removePet` action
+
[source,java]
.PetOwner_removePet.java
----
include::{examplesdir}/dom/pet/PetOwner_removePet.java[tags=barebones]
----

* `delete` action
+
[source,java]
.PetOwner_delete.java
----
include::{examplesdir}/dom/petowner/PetOwner_delete.java[tags=barebones]
----

* `bookVisit` action
+
[source,java]
.Pet_bookVisit.java
----
include::{examplesdir}/dom/visit/Pet_bookVisit.java[tags=barebones]
----

* `visits` collection
+
[source,java]
.Pet_visits.java
----
include::{examplesdir}/dom/visit/Pet_visits.java[tags=barebones]
----

[#pet]
=== Pet

The `pets.Pet` domain object (an entity) is defined as:

[source,java]
.Pet.java
----
include::{examplesdir}/dom/pet/Pet.java[tags=barebones]
----

[#petspecies]
=== PetSpecies

`PetSpecies` is an enum, in effect a value type:

[source,java]
.PetSpecies.java
----
include::{examplesdir}/dom/pet/PetSpecies.java[tags=barebones]
----

[#visit]
=== Visit

The `visits.Visit` domain object (an entity) is defined as:

[source,java]
.Visit.java
----
include::{examplesdir}/dom/visit/Visit.java[tags=barebones]
----



