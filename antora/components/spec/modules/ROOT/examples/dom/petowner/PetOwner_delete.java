package petclinic.modules.pets.dom.petowner;

import javax.inject.Inject;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.Publishing;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.events.domain.ActionDomainEvent;
import org.apache.isis.applib.services.repository.RepositoryService;

import lombok.RequiredArgsConstructor;

// tag::barebones[]
@Action(
        semantics = SemanticsOf.NON_IDEMPOTENT_ARE_YOU_SURE,
        // ...
// end::barebones[]
        domainEvent = PetOwner_delete.ActionEvent.class,
        commandPublishing = Publishing.ENABLED,
        executionPublishing = Publishing.ENABLED
// tag::barebones[]
)
// end::barebones[]
@ActionLayout(
        associateWith = "name", position = ActionLayout.Position.PANEL,
        describedAs = "Deletes this object from the persistent datastore")
@RequiredArgsConstructor
// tag::barebones[]
public class PetOwner_delete {
// end::barebones[]

    public static class ActionEvent extends ActionDomainEvent<PetOwner_delete>{}

    private final PetOwner petOwner;

// tag::barebones[]
    public void act() {
        // ...
// end::barebones[]
        repositoryService.remove(petOwner);
        return;
// tag::barebones[]
    }
// end::barebones[]

    @Inject RepositoryService repositoryService;
// tag::barebones[]
}
// end::barebones[]
