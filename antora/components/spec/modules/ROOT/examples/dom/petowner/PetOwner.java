package petclinic.modules.pets.dom.petowner;

import java.util.Comparator;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.DomainObject;
import org.apache.isis.applib.annotation.DomainObjectLayout;
import org.apache.isis.applib.annotation.Property;
import org.apache.isis.applib.annotation.PropertyLayout;
import org.apache.isis.applib.annotation.Publishing;
import org.apache.isis.applib.annotation.Where;
import org.apache.isis.applib.jaxb.PersistentEntityAdapter;
import org.apache.isis.applib.services.message.MessageService;
import org.apache.isis.applib.services.repository.RepositoryService;
import org.apache.isis.applib.services.title.TitleService;
import org.apache.isis.persistence.jpa.applib.integration.IsisEntityListener;

import static org.apache.isis.applib.annotation.SemanticsOf.IDEMPOTENT;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.val;

import petclinic.modules.pets.types.EmailAddress;
import petclinic.modules.pets.types.FirstName;
import petclinic.modules.pets.types.LastName;
import petclinic.modules.pets.types.Notes;
import petclinic.modules.pets.types.PhoneNumber;


@Entity
@Table(
    schema="pets",
    name = "PetOwner",
    uniqueConstraints = {
        @UniqueConstraint(name = "PetOwner__lastName__UNQ", columnNames = {"lastName"})
    }
)
@NamedQueries({
        @NamedQuery(
                name = PetOwner.NAMED_QUERY__FIND_BY_LAST_NAME_LIKE,
                query = "SELECT so " +
                        "FROM PetOwner so " +
                        "WHERE so.lastName LIKE :lastName"
        )
})
@EntityListeners(IsisEntityListener.class)
@DomainObjectLayout()
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@XmlJavaTypeAdapter(PersistentEntityAdapter.class)
@ToString(onlyExplicitlyIncluded = true)
// tag::barebones[]
@DomainObject(
        logicalTypeName = "pets.PetOwner",
        // ...
// end::barebones[]
        entityChangePublishing = Publishing.ENABLED
// tag::barebones[]
)
public class PetOwner implements Comparable<PetOwner> {

// end::barebones[]
    static final String NAMED_QUERY__FIND_BY_LAST_NAME_LIKE = "PetOwner.findByLastNameLike";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @Getter @Setter
    @PropertyLayout(fieldSetId = "metadata", sequence = "1")
// tag::barebones[]
    private Long id;
// end::barebones[]

    @Version
    @Column(name = "version", nullable = false)
    @PropertyLayout(fieldSetId = "metadata", sequence = "999")
    @Getter @Setter
// tag::barebones[]
    private long version;
// end::barebones[]

    public static PetOwner withName(String name) {
        return withName(name, null);
    }

    public static PetOwner withName(String lastName, String firstName) {
        val simpleObject = new PetOwner();
        simpleObject.setLastName(lastName);
        simpleObject.setFirstName(firstName);
        return simpleObject;
    }

    @Inject @Transient RepositoryService repositoryService;
    @Inject @Transient TitleService titleService;
    @Inject @Transient MessageService messageService;
// tag::barebones[]

    public String title() {
        // ...
// end::barebones[]
        return getLastName() + (getFirstName() != null ? ", " + getFirstName() : "");
// tag::barebones[]
    }

// end::barebones[]
    @Transient
    @PropertyLayout(fieldSetId = "name", sequence = "1")
    public String getName() {
        return (getFirstName() != null ? getFirstName() + " ": "")  + getLastName();
    }

    @LastName
    @Column(name = "lastName", length = LastName.MAX_LEN, nullable = false)
    @Getter @Setter @ToString.Include
    @Property(hidden = Where.EVERYWHERE)
// tag::barebones[]
    private String lastName;
// end::barebones[]

    @FirstName
    @Column(name = "firstName", length = FirstName.MAX_LEN, nullable = true)
    @Getter @Setter @ToString.Include
    @Property(hidden = Where.EVERYWHERE)
// tag::barebones[]
    private String firstName;
// end::barebones[]

    @PhoneNumber
    @Column(name = "phoneNumber", length = PhoneNumber.MAX_LEN, nullable = true)
    @PropertyLayout(fieldSetId = "contactDetails", sequence = "1")
    @Getter @Setter
// tag::barebones[]
    private String phoneNumber;
// end::barebones[]

    @EmailAddress
    @Column(name = "emailAddress", length = EmailAddress.MAX_LEN, nullable = true)
    @PropertyLayout(fieldSetId = "contactDetails", sequence = "2")
    @Getter @Setter
// tag::barebones[]
    private String emailAddress;
// end::barebones[]

    @Notes
    @Column(name = "notes", length = Notes.MAX_LEN, nullable = true)
    @Getter @Setter
    @Property(commandPublishing = Publishing.ENABLED, executionPublishing = Publishing.ENABLED)
    @PropertyLayout(fieldSetId = "notes", sequence = "1")
// tag::barebones[]
    private String notes;

// end::barebones[]

    @ActionLayout(associateWith = "name")
// tag::barebones[]
    @Action(semantics = IDEMPOTENT, commandPublishing = Publishing.ENABLED, executionPublishing = Publishing.ENABLED)
    public PetOwner updateName(
            @LastName final String lastName,
            @FirstName final String firstName) {
        // ...
// end::barebones[]
        setLastName(lastName);
        setFirstName(firstName);
        return this;
// tag::barebones[]
    }
    public String default0UpdateName() {
        // ...
// end::barebones[]
        return getLastName();
// tag::barebones[]
    }
    public String default1UpdateName() {
        // ...
// end::barebones[]
        return getFirstName();
// tag::barebones[]
    }
// end::barebones[]



    private final static Comparator<PetOwner> comparator =
            Comparator.comparing(PetOwner::getLastName);

    @Override
    public int compareTo(final PetOwner other) {
        return comparator.compare(this, other);
    }

// tag::barebones[]
}
// end::barebones[]
