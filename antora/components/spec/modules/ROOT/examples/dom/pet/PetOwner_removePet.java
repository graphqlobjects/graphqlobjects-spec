package petclinic.modules.pets.dom.pet;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.Publishing;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.services.repository.RepositoryService;

import lombok.RequiredArgsConstructor;

import petclinic.modules.pets.dom.pet.Pet;
import petclinic.modules.pets.dom.pet.PetRepository;
import petclinic.modules.pets.dom.petowner.PetOwner;
import petclinic.modules.pets.types.PetName;

@ActionLayout(associateWith = "pets", sequence = "2")
@RequiredArgsConstructor
// tag::barebones[]
@Action(
        semantics = SemanticsOf.IDEMPOTENT,
        // ...
// end::barebones[]
        commandPublishing = Publishing.ENABLED,
        executionPublishing = Publishing.ENABLED
// tag::barebones[]
)
public class PetOwner_removePet {
// end::barebones[]

    private final PetOwner petOwner;

// tag::barebones[]
    public PetOwner act(@PetName final String name) {
        // ...
// end::barebones[]
        petRepository.findByPetOwnerAndName(petOwner, name)
                .ifPresent(pet -> repositoryService.remove(pet));
        return petOwner;
// tag::barebones[]
    }
    public String disableAct() {
        // ...
// end::barebones[]
        return petRepository.findByPetOwner(petOwner).isEmpty() ? "No pets" : null;
// tag::barebones[]
    }
    public List<String> choices0Act() {
        // ...
// end::barebones[]
        return petRepository.findByPetOwner(petOwner)
                .stream()
                .map(Pet::getName)
                .collect(Collectors.toList());
// tag::barebones[]
    }
    public String default0Act() {
        // ...
// end::barebones[]
        List<String> names = choices0Act();
        return names.size() == 1 ? names.get(0) : null;
// tag::barebones[]
    }
// end::barebones[]

    @Inject PetRepository petRepository;
    @Inject RepositoryService repositoryService;
// tag::barebones[]
}
// end::barebones[]
