package petclinic.modules.pets.dom.pet;

// tag::barebones[]
public enum PetSpecies {
    Dog,
    Cat,
    Hamster,
    Budgerigar,
}
// end::barebones[]
