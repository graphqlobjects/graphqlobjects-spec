package petclinic.modules.pets.dom.pet;

import java.util.List;

import javax.inject.Inject;

import org.apache.isis.applib.annotation.Collection;
import org.apache.isis.applib.annotation.CollectionLayout;

import lombok.RequiredArgsConstructor;

import petclinic.modules.pets.dom.pet.Pet;
import petclinic.modules.pets.dom.pet.PetRepository;
import petclinic.modules.pets.dom.petowner.PetOwner;

@CollectionLayout(defaultView = "table")
@RequiredArgsConstructor
// tag::barebones[]
@Collection
public class PetOwner_pets {
// end::barebones[]

    private final PetOwner petOwner;

// tag::barebones[]
    public List<Pet> coll() {
        // ...
// end::barebones[]
        return petRepository.findByPetOwner(petOwner);
// tag::barebones[]
    }
// end::barebones[]

    @Inject PetRepository petRepository;
// tag::barebones[]
}
// end::barebones[]
