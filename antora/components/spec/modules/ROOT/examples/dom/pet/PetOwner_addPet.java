package petclinic.modules.pets.dom.pet;

import javax.inject.Inject;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.Publishing;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.services.repository.RepositoryService;

import lombok.RequiredArgsConstructor;

import petclinic.modules.pets.dom.pet.Pet;
import petclinic.modules.pets.dom.pet.PetRepository;
import petclinic.modules.pets.dom.pet.PetSpecies;
import petclinic.modules.pets.dom.petowner.PetOwner;
import petclinic.modules.pets.types.PetName;

// tag::barebones[]
@Action(
        semantics = SemanticsOf.IDEMPOTENT,
        // ...
// end::barebones[]
        commandPublishing = Publishing.ENABLED,
        executionPublishing = Publishing.ENABLED
// tag::barebones[]
)
// end::barebones[]
@ActionLayout(associateWith = "pets", sequence = "1")
@RequiredArgsConstructor
// tag::barebones[]
public class PetOwner_addPet {
// end::barebones[]

    private final PetOwner petOwner;

// tag::barebones[]
    public PetOwner act(
            @PetName final String name,
            final PetSpecies petSpecies
            ) {
        // ...
// end::barebones[]
        repositoryService.persist(new Pet(petOwner, name, petSpecies));
        return petOwner;
// tag::barebones[]
    }
    public String validate0Act(final String name) {
        // ...
// end::barebones[]
        return petRepository.findByPetOwnerAndName(petOwner, name).isPresent()
                ? String.format("Pet with name '%s' already defined for this owner", name)
                : null;
// tag::barebones[]
    }
// end::barebones[]
// tag::barebones[]
    public PetSpecies default1Act() {
        // ...
// end::barebones[]
        return PetSpecies.Dog;
// tag::barebones[]
    }
// end::barebones[]

    @Inject PetRepository petRepository;
    @Inject RepositoryService repositoryService;
// tag::barebones[]
}
// end::barebones[]
