package petclinic.modules.pets.dom.pet;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.isis.applib.annotation.DomainObject;
import org.apache.isis.applib.annotation.DomainObjectLayout;
import org.apache.isis.applib.annotation.Property;
import org.apache.isis.applib.annotation.PropertyLayout;
import org.apache.isis.applib.annotation.Publishing;
import org.apache.isis.applib.jaxb.PersistentEntityAdapter;
import org.apache.isis.persistence.jpa.applib.integration.IsisEntityListener;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import petclinic.modules.pets.dom.petowner.PetOwner;
import petclinic.modules.pets.types.FirstName;
import petclinic.modules.pets.types.Notes;
import petclinic.modules.pets.types.PetName;


@Entity
@Table(
    schema="pets",
    name = "Pet",
    uniqueConstraints = {
        @UniqueConstraint(name = "Pet__owner_name__UNQ", columnNames = {"owner_id", "name"})
    }
)
@EntityListeners(IsisEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@XmlJavaTypeAdapter(PersistentEntityAdapter.class)
@ToString(onlyExplicitlyIncluded = true)
@DomainObjectLayout()
// tag::barebones[]
@DomainObject(
        logicalTypeName = "pets.Pet",
        // ...
// end::barebones[]
        entityChangePublishing = Publishing.ENABLED
// tag::barebones[]
)
public class Pet implements Comparable<Pet> {

// end::barebones[]
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @Getter @Setter
    @PropertyLayout(fieldSetId = "metadata", sequence = "1")
// tag::barebones[]
    private Long id;
// end::barebones[]

    @Version
    @Column(name = "version", nullable = false)
    @PropertyLayout(fieldSetId = "metadata", sequence = "999")
    @Getter @Setter
// tag::barebones[]
    private long version;
// end::barebones[]


    Pet(PetOwner petOwner, String name, PetSpecies petSpecies) {
        this.petOwner = petOwner;
        this.name = name;
        this.petSpecies = petSpecies;
    }

// tag::barebones[]

    public String title() {
        // ...
// end::barebones[]
        return getName() + " " + getPetOwner().getLastName();
// tag::barebones[]
    }
// end::barebones[]

    public String iconName() {
        return getPetSpecies().name().toLowerCase();
    }

    @PetName
    @Column(name = "name", length = FirstName.MAX_LEN, nullable = false)
    @Getter @Setter
    @PropertyLayout(fieldSetId = "name", sequence = "2")
// tag::barebones[]
    private String name;
// end::barebones[]

    @Notes
    @Column(name = "notes", length = Notes.MAX_LEN, nullable = true)
    @Getter @Setter
    @Property(commandPublishing = Publishing.ENABLED, executionPublishing = Publishing.ENABLED)
    @PropertyLayout(fieldSetId = "notes", sequence = "1")
// tag::barebones[]
    private String notes;
// end::barebones[]

    @ManyToOne(optional = false)
    @JoinColumn(name = "owner_id")
    @PropertyLayout(fieldSetId = "name", sequence = "1")
    @Getter @Setter
// tag::barebones[]
    private PetOwner petOwner;
// end::barebones[]

    @Enumerated(EnumType.STRING)
    @Column(name = "petSpecies", nullable = false)
    @Getter @Setter
    @PropertyLayout(fieldSetId = "details", sequence = "1")
// tag::barebones[]
    private PetSpecies petSpecies;
// end::barebones[]


    private final static Comparator<Pet> comparator =
            Comparator.comparing(Pet::getPetOwner).thenComparing(Pet::getName);

    @Override
    public int compareTo(final Pet other) {
        return comparator.compare(this, other);
    }

// tag::barebones[]
}
// end::barebones[]
