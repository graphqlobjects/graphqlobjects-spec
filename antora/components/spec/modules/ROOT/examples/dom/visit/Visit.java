package petclinic.modules.visits.dom.visit;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.isis.applib.annotation.DomainObject;
import org.apache.isis.applib.annotation.DomainObjectLayout;
import org.apache.isis.applib.annotation.Property;
import org.apache.isis.applib.annotation.PropertyLayout;
import org.apache.isis.applib.annotation.Publishing;
import org.apache.isis.applib.jaxb.PersistentEntityAdapter;
import org.apache.isis.applib.services.title.TitleService;
import org.apache.isis.persistence.jpa.applib.integration.IsisEntityListener;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import petclinic.modules.pets.dom.pet.Pet;
import petclinic.modules.pets.dom.pet.PetSpecies;
import petclinic.modules.pets.dom.petowner.PetOwner;
import petclinic.modules.pets.types.FirstName;
import petclinic.modules.pets.types.Notes;
import petclinic.modules.pets.types.PetName;
import petclinic.modules.visits.types.Reason;


@Entity
@Table(
    schema="visits",
    name = "Visit",
    uniqueConstraints = {
        @UniqueConstraint(name = "Visit__pet_visitAt__UNQ", columnNames = {"owner_id", "name"})
    }
)
@EntityListeners(IsisEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@XmlJavaTypeAdapter(PersistentEntityAdapter.class)
@ToString(onlyExplicitlyIncluded = true)
// tag::barebones[]
@DomainObject(
        logicalTypeName = "visits.Visit",
        // ...
// end::barebones[]
        entityChangePublishing = Publishing.ENABLED
// tag::barebones[]
)
// end::barebones[]
@DomainObjectLayout()
// tag::barebones[]
public class Visit implements Comparable<Visit> {

// end::barebones[]
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @Getter @Setter
    @PropertyLayout(fieldSetId = "metadata", sequence = "1")
// tag::barebones[]
    private Long id;
// end::barebones[]

    @Version
    @Column(name = "version", nullable = false)
    @PropertyLayout(fieldSetId = "metadata", sequence = "999")
    @Getter @Setter
// tag::barebones[]
    private long version;

// end::barebones[]

    public Visit(Pet pet, LocalDateTime visitAt, String reason) {
        this.pet = pet;
        this.visitAt = visitAt;
        this.reason = reason;
    }


// tag::barebones[]
    public String title() {
        // ...
// end::barebones[]
        return titleService.titleOf(getPet()) + " @ " + getVisitAt().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
// tag::barebones[]
    }

// end::barebones[]
    @ManyToOne(optional = false)
    @JoinColumn(name = "pet_id")
    @PropertyLayout(fieldSetId = "name", sequence = "1")
    @Getter @Setter
// tag::barebones[]
    private Pet pet;
// end::barebones[]

    @Column(name = "visitAt", nullable = false)
    @Getter @Setter
    @PropertyLayout(fieldSetId = "name", sequence = "2")
// tag::barebones[]
    private LocalDateTime visitAt;
// end::barebones[]

    @Reason
    @Column(name = "reason", length = FirstName.MAX_LEN, nullable = false)
    @Getter @Setter
    @PropertyLayout(fieldSetId = "details", sequence = "1")
// tag::barebones[]
    private String reason;
// end::barebones[]

    private final static Comparator<Visit> comparator =
            Comparator.comparing(Visit::getPet).thenComparing(Visit::getVisitAt);

    @Override
    public int compareTo(final Visit other) {
        return comparator.compare(this, other);
    }

    @Inject @Transient TitleService titleService;
// tag::barebones[]
}
// end::barebones[]
