package petclinic.modules.visits.contributions.pet;

import java.util.List;

import javax.inject.Inject;

import org.apache.isis.applib.annotation.Collection;
import org.apache.isis.applib.annotation.CollectionLayout;

import lombok.RequiredArgsConstructor;

import petclinic.modules.pets.dom.pet.Pet;
import petclinic.modules.pets.dom.pet.PetRepository;
import petclinic.modules.pets.dom.petowner.PetOwner;
import petclinic.modules.visits.dom.visit.Visit;
import petclinic.modules.visits.dom.visit.VisitRepository;

// tag::barebones[]
@Collection
// end::barebones[]
@CollectionLayout(defaultView = "table")
@RequiredArgsConstructor
// tag::barebones[]
public class Pet_visits {

// end::barebones[]
    private final Pet pet;

// tag::barebones[]
    public List<Visit> coll() {
        // ...
// end::barebones[]
        return visitRepository.findByPetOrderByVisitAtDesc(pet);
// tag::barebones[]
    }
// end::barebones[]

    @Inject VisitRepository visitRepository;
// tag::barebones[]
}
// end::barebones[]
