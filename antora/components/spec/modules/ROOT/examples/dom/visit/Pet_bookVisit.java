package petclinic.modules.visits.contributions.pet;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import javax.inject.Inject;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.Publishing;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.services.clock.ClockService;
import org.apache.isis.applib.services.repository.RepositoryService;

import lombok.RequiredArgsConstructor;

import petclinic.modules.pets.dom.pet.Pet;
import petclinic.modules.pets.dom.pet.PetRepository;
import petclinic.modules.pets.dom.pet.PetSpecies;
import petclinic.modules.pets.dom.petowner.PetOwner;
import petclinic.modules.pets.types.PetName;
import petclinic.modules.visits.dom.visit.Visit;
import petclinic.modules.visits.types.Reason;

@RequiredArgsConstructor
// tag::barebones[]
@Action(
        semantics = SemanticsOf.IDEMPOTENT,
        // ...
// end::barebones[]
        commandPublishing = Publishing.ENABLED,
        executionPublishing = Publishing.ENABLED
// tag::barebones[]
)
// end::barebones[]
@ActionLayout(associateWith = "visits", sequence = "1")
// tag::barebones[]
public class Pet_bookVisit {

// end::barebones[]
    private final Pet pet;

// tag::barebones[]
    public Visit act(
            LocalDateTime visitAt,
            @Reason final String reason
            ) {
        // ...
// end::barebones[]
        return repositoryService.persist(new Visit(pet, visitAt, reason));
// tag::barebones[]
    }
    public String validate0Act(LocalDateTime visitAt) {
        // ...
// end::barebones[]
        return clockService.getClock().nowAsLocalDateTime().isBefore(visitAt)
                ? null
                : "Must be in the future";
// tag::barebones[]
    }
    public LocalDateTime default0Act() {
        // ...
// end::barebones[]
        return clockService.getClock().nowAsLocalDateTime()
                .toLocalDate()
                .plusDays(1)
                .atTime(LocalTime.of(9, 0));
// tag::barebones[]
    }
// end::barebones[]

    @Inject ClockService clockService;
    @Inject RepositoryService repositoryService;
// tag::barebones[]
}
// end::barebones[]
